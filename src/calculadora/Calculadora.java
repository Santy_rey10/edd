package calculadora;

import java.util.Scanner;

/**
 * @author @author Santiago Reynoso, Nadia Morel & Nicolas Aguilar
 */
public class Calculadora {

    public static void main(String[] args) {
        //Variables
        Scanner sc = new Scanner(System.in);
        double resultado = 0;
        int n = 0;
        String operacion;
        String numero2 = null;
        boolean comprobar = false;
        boolean comprobar1 = true;
        boolean comprobar2 = true;
        boolean rip = false;

        //Inicio
        System.out.println("Calculadora:");
        do {
            // Verificación de los datos puestos por el usuario del número 1.
            String numero1;
            do {
                System.out.println("\n Ingrece el primer número de la operación. ");
                numero1 = sc.nextLine();
            } while (!numero1.matches("[+-]?[\\d]*[.]?[\\d]+"));
            double nume1 = Double.parseDouble(numero1);
            double n1 = new Double(numero1);
            // Fin de la verificación de los datos puestos por el usuario del número 1.

            // Verificamos que operación desea realizar
            do {
                System.out.println("\n ¿Que operación desea hacer? (Solo coloque un signo)");
                System.out.println("Teniendo en cuenta que: \n + = Sumar \n - = Restar \n"
                        + " * = Multiplicar \n / = Dividir \n +- = Par o Impar \n *2 =  Potencia al cuadrado");
                operacion = sc.nextLine();
                if (operacion.equals("+") || operacion.equals("-") || operacion.equals("x")
                        || operacion.equals("X") || operacion.equals("/") || operacion.equals("*")) {
                    comprobar = true;
                } else {
                    if (operacion.equals("+-")) {
                        comprobar = true;
                        comprobar1 = false;
                        rip = true;
                    } else {
                        if (operacion.equals("*2")) {
                            comprobar = true;
                            comprobar2 = false;
                            rip = true;
                        }
                    }
                }
            } while (comprobar != true);
            // Fin de la verificación de operación

            // Verificación de los datos puestos por el usuario del número 2.            
            if (rip != true) {
                do {
                    System.out.println("\n Ingrece el segundo número.");
                    numero2 = sc.nextLine();
                } while (!numero2.matches("[+-]?[\\d]*[.]?[\\d]+") && (comprobar != true));
            }
            double nume2 = Double.parseDouble(numero2);
            double n2 = new Double(numero2);

            // Fin de la verificación de los datos puestos por el usuario del número 2.
            do {
                comprobar = true;
                //Operaciones de calculo
                switch (operacion) {
                    case "+":
                        resultado = n1 + n2;
                        break;
                    case "-":
                        resultado = n1 - n2;
                        break;
                    case "*":
                        resultado = n1 * n2;
                        break;
                    case "/":
                        /* Por tal de evitar errores y añadir números complejos, si el usuario coloca 0 como segundo número, se piensa entonces que el denominador es 0, y por ello añado una condicional que lo verifique, y para hacer el codigo mas dinamico y no solo añadir un 1, le vuelvo a preguntar al usuario que añada un número distinto, podría volver a colocar 0, y es la razón por la que se encuentra en un ciclo, así que mientras n2 sea 0, el código seguirá ejecutando hasta que ponga otro. */
                        while (n2 == 0) {
                            do {
                                System.err.println(" En el denominador se encuentra \n"
                                        + "un cero, para evitar errores coloca otro número.");
                                numero2 = sc.nextLine();
                            } while (!numero2.matches("[+-]?[\\d]*[.]?[\\d]+"));
                            nume2 = Double.parseDouble(numero2);
                            n2 = new Double(numero2);
                        }
                        resultado = n1 / n2;
                        break;
                }
                //Fin de los calculos
            } while (comprobar != true);

            //Resultado
            if (comprobar1 != true) {
                double Aux;
                Aux = n1 % 2;
                if (Aux == 0) {
                    System.err.println("\nEl numero " + n1 + " es un numero Par");
                } else {
                    System.err.println("\nEl numero " + n1 + " es un numero Impar");
                }
            } //else {
                if (comprobar2 != true) {
                    double Aux = n1;
                    double total = n1;
                    for (int i = 1; i < Aux; i++) {
                        total = total * n1;
                    }
                    System.err.println("\nEl resultado es: " + total);
                } else {
                    System.out.println("(" + numero1 + ") " + operacion + " (" + numero2 + ")" + " = " + resultado);
                }
            //}
            //Fin del Resultado

            //Final de la operacion
            System.out.println("\n ¿Desea hacer alguna otra operación? \n");
            System.out.println(" [s/n]");
            do {
                comprobar = true;
                operacion = sc.nextLine();

                switch (operacion) {
                    case "s":
                    case "S":
                    case "n":
                    case "N":
                        break;
                    default:
                        System.err.println("\n Error, ponga una opcion valida. \n");
                        comprobar = false;
                }
            } while (comprobar != true);
        } while (operacion.equals("s") || operacion.equals("S"));

    }
}
//Fin
